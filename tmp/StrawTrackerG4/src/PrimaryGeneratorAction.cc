#include "PrimaryGeneratorAction.hh"

#include "DetectorConstruction.hh"
#include "PrimaryGeneratorMessenger.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  G4int n_particle = 1;
  particleGun  = new G4ParticleGun(n_particle);
  Detector = (DetectorConstruction*)
             G4RunManager::GetRunManager()->GetUserDetectorConstruction();  
  
  //create a messenger for this class
  gunMessenger = new PrimaryGeneratorMessenger(this);

  // default particle kinematic

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;

  G4ParticleDefinition* particle
                    = particleTable->FindParticle(particleName="e-");

  particleGun->SetParticleDefinition(particle);
  particleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));

  ////////////////////////////////////////////////
  //           beam energy !!!!!!!             ///
  ////////////////////////////////////////////////

  particleGun->SetParticleEnergy(200*MeV);

  ////////////////////////////////////////////////

  G4double position = -0.5*(Detector->GetWorldSizeX());
  particleGun->SetParticlePosition(G4ThreeVector(0*cm,0.*cm,100.*cm));

  
  rndmFlag = "off";

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
  delete gunMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of event
  // 
  G4double x0 = -0.5*(Detector->GetWorldSizeX());
  G4double y0 = 0.*cm, z0 = 0.*cm;
  if (rndmFlag == "on")
     {y0 = (Detector->GetCalorSizeYZ())*(G4UniformRand()-0.5);
      z0 = (Detector->GetCalorSizeYZ())*(G4UniformRand()-0.5);
     } 

  x0=0.*cm;
  y0=0.*cm;
  z0=-100.*cm;

  G4double tmpr = 3.*G4UniformRand();
  G4double tmpr2 = 3./2*(G4UniformRand()-0.5);
  G4double tmptheta = 2*3.14159265358979323846*G4UniformRand();
  x0 = tmpr*cos(tmptheta);
  y0 = tmpr*sin(tmptheta);
 
  //x0 = tmpr; y0 = tmpr2;
  //x0 = 0; y0 = 0;
 
  particleGun->SetParticlePosition(G4ThreeVector(x0*cm,y0*cm,z0));
  G4double pi = 3.14159265358979323846;
  //  G4double r = 2*G4UniformRand()-1;
  G4double r = 1-0.5*G4UniformRand();
  //G4double r = G4UniformRand();
  //G4double r = 0.89*G4UniformRand();
  G4double phi = 2*pi*G4UniformRand();
  G4double px = sqrt(1-r*r)*cos(phi);
  G4double py = sqrt(1-r*r)*sin(phi);
  G4double pz = r;

  //  particleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  particleGun->SetParticleMomentumDirection(G4ThreeVector(px,py,pz));


  G4double Me = 0.510998910; //MeV
  G4double Pe = 105; //MeV/c
  G4double Ee = sqrt(Pe*Pe+Me*Me);
  G4double Ke = Ee-Me;

  particleGun->SetParticleEnergy(Ke*MeV);

  particleGun->GeneratePrimaryVertex(anEvent);


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

