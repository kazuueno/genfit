#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "MField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4ChordFinder.hh"


#include "G4UserLimits.hh"//To apply steplength upper limit hiromi


DetectorConstruction::DetectorConstruction()
:AbsorberMaterial(0),GapMaterial(0),defaultMaterial(0),
 solidWorld(0),logicWorld(0),physiWorld(0)
 //magField(0)
{


  ////////////////////////////////////////////////////////
  //                    change free                      //
  ////////////////////////////////////////////////////////

  // default parameter values of the calorimeter
  AbsorberThickness = 0.3*mm;
  //GapThickness      = 10.*mm;
  //NbOfLayers        = 3;
  CalorSizeYZ       = 1.*cm;
  //ComputeCalorParameters();
  NStations = 5;
  WallThickness=0.025;
  // materials
  DefineMaterials();
  //SetAbsorberMaterial("SiDetector");
  SetGapMaterial("Vacuum");
  SetDiskMaterial("Mylar");
  SetGasMaterial("Argon50_Ethane50");
  //  SetGasMaterial("Helium50_Ethane50");
/////////////////////////////////////////////////////////


  // create commands for interactive definition of the calorimeter
  detectorMessenger = new DetectorMessenger(this);
}


DetectorConstruction::~DetectorConstruction()
{ delete detectorMessenger;}


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  return ConstructCalorimeter();
}


void DetectorConstruction::DefineMaterials()
{ 
 //This function illustrates the possible ways to define materials
 
G4String symbol;             //a=mass of a mole;
G4double a, z, density;      //z=mean number of protons;  
G4int iz, n;                 //iz=number of protons  in an isotope; 
                             // n=number of nucleons in an isotope;

G4int ncomponents, natoms;
G4double abundance, fractionmass;

//
// define Elements
//

G4Element* H  = new G4Element("Hydrogen",symbol="H" , z= 1., a= 1.01*g/mole);
G4Element* He  = new G4Element("Helium",symbol="He" , z= 2., a= 4.0026*g/mole);
G4Element* C  = new G4Element("Carbon"  ,symbol="C" , z= 6., a= 12.01*g/mole);
G4Element* N  = new G4Element("Nitrogen",symbol="N" , z= 7., a= 14.01*g/mole);
G4Element* O  = new G4Element("Oxygen"  ,symbol="O" , z= 8., a= 16.00*g/mole);
G4Element* F = new G4Element("Fluorine"  , "F", z=9., a= 18.99*g/mole);
G4Element* Si = new G4Element("Silicon",symbol="Si" , z= 14., a= 28.09*g/mole);
 
G4Element* Ar = new G4Element("Argon"  , "Ar", z=18., a= 39.95*g/mole);
G4Element* Xe = new G4Element("Xenon "  , "Xe", z=54., a= 131.29*g/mole);
G4Element* Ge   = new G4Element("Germanium"  , "Ge", z=32, a= 72.64*g/mole);
G4Element* Br = new G4Element("Bromine"  , "Br", z=35, a= 79.90*g/mole);
G4Element* Gd = new G4Element("Gadolinium"  , "Gd", z=64, a= 157.25*g/mole);
G4Element* B = new G4Element("Boron"  , "B", z=5., a=10.81*g/mole);
G4Element* Na = new G4Element("Sodium", "Na", z= 11, a= 22.99*g/mole);
G4Element* I  = new G4Element("Iodine  ", "I", z= 53, a=126.90*g/mole);
G4Element* W = new G4Element("Tungsten_el"  , "W", z=74., a= 183.84*g/mole);
//
// define an Element from isotopes, by relative abundance 
//

G4Isotope* U5 = new G4Isotope("U235", iz=92, n=235, a=235.01*g/mole);
G4Isotope* U8 = new G4Isotope("U238", iz=92, n=238, a=238.03*g/mole);

G4Element* U  = new G4Element("enriched Uranium",symbol="U",ncomponents=2);
U->AddIsotope(U5, abundance= 90.*perCent);
U->AddIsotope(U8, abundance= 10.*perCent);

//
// define simple materials
//

 new G4Material("Aluminium", z=13., a=26.98*g/mole, density=2.700*g/cm3);
 new G4Material("liquidArgon", z=18., a= 39.95*g/mole, density= 1.390*g/cm3);
 new G4Material("Lead"     , z=82., a= 207.19*g/mole, density= 11.35*g/cm3);
 new G4Material("SiDetector"  , z=14., a= 28.09*g/mole, density= 2.33*g/cm3);
 G4Material* Ar_gas = 
   new G4Material("Ar_gas", z=18, a= 39.948*g/mole, density= 1.65*mg/cm3,
		  kStateGas, 294.1*kelvin,1.*atmosphere );
 G4Material* Xe_gas =
   new G4Material("Xe_gas", z=54, a= 131.29*g/mole, density= 5.472*mg/cm3,
		  kStateGas, 294.1*kelvin, 1.*atmosphere);
 G4Material* He_gas =
   new G4Material("He_gas", z=1, a=4.0026*g/mole, density=0.165*mg/cm3,
		  kStateGas, 294.1*kelvin, 1.*atmosphere); 

new G4Material("Copper"     , z=29., a= 63.546*g/mole, density= 8.93*g/cm3);
new G4Material("TungstenS"     , z=74., a= 183.84*g/mole, density= 19.3*g/cm3);
 

//
// define a material from elements.   case 1: chemical molecule
//

G4Material* H2O = 
new G4Material("Water", density= 1.000*g/cm3, ncomponents=2);
H2O->AddElement(H, natoms=2);
H2O->AddElement(O, natoms=1);
// overwrite computed meanExcitationEnergy with ICRU recommended value 
H2O->GetIonisation()->SetMeanExcitationEnergy(78.0*eV);

G4Material* Sci = 
new G4Material("Scintillator", density= 1.032*g/cm3, ncomponents=2);
Sci->AddElement(C, natoms=9);
Sci->AddElement(H, natoms=10);

G4Material* Myl = 
new G4Material("Mylar", density= 1.397*g/cm3, ncomponents=3);
Myl->AddElement(C, natoms=10);
Myl->AddElement(H, natoms= 8);
Myl->AddElement(O, natoms= 4);

G4Material* SiO2 = 
new G4Material("quartz",density= 2.200*g/cm3, ncomponents=2);
SiO2->AddElement(Si, natoms=1);
SiO2->AddElement(O , natoms=2);

G4Material* Polyimide = new G4Material("Polyimide", density= 1.47*g/cm3, ncomponents=4);
 Polyimide->AddElement(C, natoms=14);
 Polyimide->AddElement(H, natoms= 22);
 Polyimide->AddElement(O, natoms= 4);
 Polyimide->AddElement(N, natoms= 2);

G4Material* Epoxy = new G4Material("Epoxy", density= 1.7*g/cm3 ,ncomponents=3);
 Epoxy->AddElement(C, natoms=10);
 Epoxy->AddElement(H, natoms=10);
 Epoxy->AddElement(O, natoms= 2);

//
// define a material from elements.   case 2: mixture by fractional mass
//

G4Material* Air = 
new G4Material("Air"  , density= 1.290*mg/cm3, ncomponents=2);
Air->AddElement(N, fractionmass=0.7);
Air->AddElement(O, fractionmass=0.3);

//
// examples of gas in non STP conditions
//

G4Material* CO2 = 
new G4Material("CO2", density= 1.842*mg/cm3, ncomponents=2,
                              kStateGas, 325.*kelvin, 50.*atmosphere);
CO2->AddElement(C, natoms=1);
CO2->AddElement(O, natoms=2);
 
G4Material* steam = 
new G4Material("WaterSteam", density= 0.3*mg/cm3, ncomponents=1,
                             kStateGas, 500.*kelvin, 2.*atmosphere);
steam->AddMaterial(H2O, fractionmass=1.);

G4Material* Ethane = new G4Material("Ethane", density= 1.28*mg/cm3, ncomponents=2,
				     kStateGas, 394.1*kelvin, 1.*atmosphere);
 Ethane->AddElement(C, natoms=2);
 Ethane->AddElement(H, natoms=6);

G4Material* CF4 = 
   new G4Material("CF4", density= 3.65*mg/cm3, ncomponents=2, kStateGas,
		  294.1*kelvin, 1.*atmosphere);
 CF4->AddElement(C, natoms=1);
 CF4->AddElement(F, natoms=4);

//
// define a material from elements and/or others materials (mixture of mixtures)
//

G4Material* Aerog = 
new G4Material("Aerogel", density= 0.200*g/cm3, ncomponents=3);
Aerog->AddMaterial(SiO2, fractionmass=62.5*perCent);
Aerog->AddMaterial(H2O , fractionmass=37.4*perCent);
Aerog->AddElement (C   , fractionmass= 0.1*perCent);

G4Material* G10_Plate = 
  new G4Material("G10_Plate", density= 1.7*g/cm3, ncomponents=2);
 G10_Plate->AddMaterial(SiO2, fractionmass= 60*perCent);
 G10_Plate->AddMaterial(Epoxy, fractionmass=40*perCent);

G4Material* CFRP =
  new G4Material("CFRP", density=1.58*g/cm3, ncomponents=2);
 CFRP->AddMaterial(Epoxy, fractionmass=34*perCent);
 CFRP->AddElement(C, fractionmass=66*perCent);

G4Material* ArEth = new G4Material("Argon50_Ethane50", density= 1.465*mg/cm3, 
				   ncomponents=2, kStateGas,
				   294.1*kelvin, 1.*atmosphere);
 ArEth->AddMaterial(Ar_gas, fractionmass=56.31*perCent);
 ArEth->AddMaterial(Ethane, fractionmass=43.69*perCent);
 G4Material* HeEth = new G4Material("Helium50_Ethane50", density= 0.7225*mg/cm3,
				    ncomponents=2, kStateGas,
				    294.1*kelvin, 1.*atmosphere);
 HeEth->AddMaterial(He_gas, fractionmass=11.42*perCent);
 HeEth->AddMaterial(Ethane, fractionmass=88.58*perCent);  

//
// examples of vacuum
//

G4Material* Vacuum =
new G4Material("Vacuum", z=1., a=1.01*g/mole,density= universe_mean_density,
                           kStateGas, 2.73*kelvin, 3.e-18*pascal);

G4Material* beam = 
new G4Material("Beam", density= 1.e-5*g/cm3, ncomponents=1,
                       kStateGas, STP_Temperature, 2.e-2*bar);
beam->AddMaterial(Air, fractionmass=1.);

//
// or use G4-NIST materials data base
//
G4NistManager* man = G4NistManager::Instance();
man->FindOrBuildMaterial("G4_SODIUM_IODIDE");

// print table
//
G4cout << *(G4Material::GetMaterialTable()) << G4endl;

//default materials of the World
defaultMaterial  = Vacuum;
}



G4VPhysicalVolume* DetectorConstruction::ConstructCalorimeter()
{

  // Clean old geometry, if any
  //
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  // complete the Calor parameters definition
  //  ComputeCalorParameters();

  // Define rotation
  G4RotationMatrix* rotX = new G4RotationMatrix();
  G4RotationMatrix* rotY = new G4RotationMatrix();
  G4RotationMatrix* rotZ = new G4RotationMatrix();
  rotX->rotateX(90*deg);
  rotY->rotateY(90*deg);
  rotZ->rotateZ(90*deg);


   
  //     
  // World
  //
  solidWorld = new G4Box("World",				//its name
                   10./2*m,10./2*m,10./2*m);	//its size
                         
  logicWorld = new G4LogicalVolume(solidWorld,		//its solid
                                   defaultMaterial,	//its material
                                   "World");		//its name
                                   
  physiWorld = new G4PVPlacement(0,			//no rotation
  				 G4ThreeVector(),	//at (0,0,0)
                                 logicWorld,		//its logical volume				 
                                 "World",		//its name
                                 0,			//its mother  volume
                                 false,			//no boolean operation
                                 0);			//copy number
  
  // Solenoid
  sol_sol=0; log_sol=0; phys_sol=0;
  sol_sol = new G4Tubs("sol",0,1.68/2*m,5.*m,0.*deg,360.*deg);
  log_sol = new G4LogicalVolume(sol_sol,
				defaultMaterial,
				"sol");
  phys_sol = new G4PVPlacement(0,
			       G4ThreeVector(0,0,0),
			       log_sol,
			       "sol",
			       logicWorld,
			       false,
			       0);

  // Station
  for(int ii=0; ii<NStations; ii++){
    sol_station=0; log_station=0; phys_station[ii]=0;
  }
  sol_station = new G4Tubs("station",0,1.3/2*m,60./2*mm,0.*deg,360.*deg);
  log_station = new G4LogicalVolume(sol_station,
				    defaultMaterial,
				    "station");
  for(int ii=0; ii<NStations; ii++){
      phys_station[ii] = new G4PVPlacement(0,
					   G4ThreeVector(0,0,(G4double)ii*480*mm),
					   log_station,
					   "station",
					   log_sol,
					   false,
					   ii);
  }


  // Tube assembly
  G4double rtube=0;
  //  G4int tubenum=96;
  //G4int tubenum=168;
  G4double rin_tube=9.75;//diameter
  double   rwire=0.0125;//radius
  G4double rout_tube=rin_tube+WallThickness*2;
  G4double gap=1.;//1.5mm gap
  G4int tubenum = (int)sqrt((540*540-187*187)/((rout_tube+gap)*(rout_tube+gap)))*2;

  std::cout<<"Will construct "<<NStations<<" number of stations "<<std::endl;
  std::cout<<"wall thickness "<<WallThickness<<std::endl;
  std::cout<<"gas material "<<GasMaterial->GetName()<<std::endl;
  

  sol_asm=0; log_asm=0; 
  for(int ii=0; ii<4; ii++){
    phys_asm[ii]=0;
  }
  sol_asm = new G4Tubs("asm",0,1200./2*mm,(rout_tube)/2*mm,0.*deg,360.*deg);
  log_asm = new G4LogicalVolume(sol_asm,
				defaultMaterial,
				"asm");
  phys_asm[0] = new G4PVPlacement(0,
				  G4ThreeVector(0,0,-(1.5*gap+rout_tube+rout_tube/2)*mm),
				  log_asm,
				  "asm",
				  log_station,
				  false,
				  0);
  phys_asm[1] = new G4PVPlacement(0,
				  G4ThreeVector((gap+rout_tube)/2,0,-(0.5*gap+rout_tube/2)*mm),
				  log_asm,
				  "asm2",
				  log_station,
				  false,
				  1);
  phys_asm[2] = new G4PVPlacement(rotZ,
				  G4ThreeVector(0,0,(0.5*gap+rout_tube/2)*mm),
				  log_asm,
				  "asm3",
				  log_station,
				  false,
				  2);
  phys_asm[3] = new G4PVPlacement(rotZ,
				  G4ThreeVector(0,(gap+rout_tube)/2,(1.5*gap+rout_tube+rout_tube/2)*mm),
				  log_asm,
				  "asm4",
				  log_station,
				  false,
				  3);


  // Tube

  for(int ii=0; ii<tubenum; ii++){
    sol_tube[ii]=0; log_tube[ii]=0; phys_tube[ii]=0;
    sol_gas[ii]=0; log_gas[ii]=0; phys_gas[ii]=0;
  }

  for(int ii=0; ii<(tubenum/2); ii++){
    rtube = sqrt(540*540-(G4double)(rout_tube+gap)*(rout_tube+gap)*ii*ii);
    //G4cout << "r_tube = " << rtube << G4endl;
    sol_tube[ii] = new G4Tubs("tube",0,rout_tube/2*mm,rtube*mm,0.*deg,360.*deg);
    log_tube[ii] = new G4LogicalVolume(sol_tube[ii],
				       DiskMaterial,
				       "tube");
    phys_tube[ii] = new G4PVPlacement(rotX,
				      G4ThreeVector((0.5*gap+rout_tube/2+(rout_tube+gap)*ii)*mm,
						    0,
						    0),
				      //-(1.5+rout_tube+rout_tube/2)*mm),
				      log_tube[ii],
				      "tube",
				      log_asm,
				      false,
				      ii);

    // Gas region
    sol_gas[ii]  = new G4Tubs("gas",0,rin_tube/2*mm,(rtube-gap)*mm,0.*deg,360.*deg);
    log_gas[ii] = new G4LogicalVolume(sol_gas[ii],
				  GasMaterial,
				  "gas");
    
    phys_gas[ii] = new G4PVPlacement(0,
				 G4ThreeVector(0,0,0),
				 log_gas[ii],
				 "gas",
				 log_tube[ii],
				 false,
				 0);
    //wire 
    G4Tubs *sol_wire=new G4Tubs("wire",0,rwire*mm,(rtube-gap)*mm,0.*deg,360.*deg);
    G4LogicalVolume *log_wire=new G4LogicalVolume(sol_wire,G4Material::GetMaterial("TungstenS"),"wire");
    //    new G4PVPlacement(0,G4ThreeVector(0,0,0),log_wire,"wire",log_gas[ii],false,0);
  }

  for(int ii=(tubenum/2); ii<tubenum; ii++){
    rtube = sqrt(540*540-(G4double)(rout_tube+gap)*(rout_tube+gap)*(ii-(tubenum/2))*(ii-(tubenum/2)));
    sol_tube[ii] = new G4Tubs("tube",0,rout_tube/2*mm,rtube*mm,0.*deg,360.*deg);
    log_tube[ii] = new G4LogicalVolume(sol_tube[ii],
				       DiskMaterial,
				       "tube");
    phys_tube[ii] = new G4PVPlacement(rotX,
				      G4ThreeVector(-(0.5*gap+rout_tube/2+(rout_tube+gap)*(ii-(tubenum/2)))*mm,
						    0,
						    0),
				      //-(1.5+rout_tube+rout_tube/2)*mm),
				      log_tube[ii],
				      "tube",
				      log_asm,
				      false,
				      ii);
    // Gas region
    sol_gas[ii]  = new G4Tubs("gas",0,rin_tube/2*mm,(rtube-gap)*mm,0.*deg,360.*deg);
    log_gas[ii] = new G4LogicalVolume(sol_gas[ii],
				  GasMaterial,
				  "gas");
    
    phys_gas[ii] = new G4PVPlacement(0,
				 G4ThreeVector(0,0,0),
				 log_gas[ii],
				 "gas",
				 log_tube[ii],
				 false,
				 0);
    
    //wire 
    G4Tubs *sol_wire=new G4Tubs("wire",0,rwire*mm,(rtube-gap)*mm,0.*deg,360.*deg);
    G4LogicalVolume *log_wire=new G4LogicalVolume(sol_wire,G4Material::GetMaterial("TungstenS"),"wire");
    //new G4PVPlacement(0,G4ThreeVector(0,0,0),log_wire,"wire",log_gas[ii],false,0);


  }



  //                               
  // Absorber
  //
  /*
  solidAbsorber=0; logicAbsorber=0; physiAbsorber=0;  
  
  if (AbsorberThickness > 0.) 
    { solidAbsorber = new G4Box("Absorber",		//its name
			   AbsorberThickness/2,CalorSizeYZ/2,CalorSizeYZ/2); 

                          
      logicAbsorber = new G4LogicalVolume(solidAbsorber,    //its solid
      			                  AbsorberMaterial, //its material
      			                  AbsorberMaterial->GetName()); //name
      		                  
      physiAbsorber = new G4PVPlacement(0,		   //no rotation
      		    G4ThreeVector(0.,0.,0.),  //its position
                                        logicAbsorber,     //its logical volume		    
                                        AbsorberMaterial->GetName(), //its name
                                        logicWorld,        //its mother
                                        false,             //no boulean operat
                                        0);                //copy number
                                      
    }
  */
  // Set Mag field

  MField* myfield = new MField;
  G4FieldManager* fieldMgr
   = G4TransportationManager::GetTransportationManager()->GetFieldManager();  

  fieldMgr->SetDetectorField(myfield);
  fieldMgr->CreateChordFinder(myfield);

  G4cout << sqr(4) << " " << sqrt(4) << G4endl;

  //   SetMagField(1);


  ////////////// step limit !!! ////////////// 2013/01/25 //
  G4UserLimits* stepLimit;
  stepLimit = new  G4UserLimits(1.0*mm);
  for(int ii=0; ii<tubenum; ii++){
    log_gas[ii]->SetUserLimits(stepLimit);
    log_tube[ii]->SetUserLimits(stepLimit);
  }
  G4UserLimits* stepLimit2;
  stepLimit2 = new  G4UserLimits(10.*mm);
  log_sol->SetUserLimits(stepLimit2);

  //////////////////////////////////////////////////////////

  //PrintCalorParameters();     
  
  //                                        
  // Visualization attributes
  //
  logicWorld->SetVisAttributes (G4VisAttributes::Invisible);
  //logicLayer->SetVisAttributes (G4VisAttributes::Invisible);
  //logicGap->SetVisAttributes (G4VisAttributes::Invisible);
  //logicCalor->SetVisAttributes (G4VisAttributes::Invisible);
  G4VisAttributes *disk_col = new G4VisAttributes(G4Color(0, 1.0, 1.0));
  G4VisAttributes *tube_col = new G4VisAttributes(G4Color(1.0, 1.0, 0.0));
  G4VisAttributes *gas_col = new G4VisAttributes(G4Color(1.0, 0, 0.0));
  //  log_station->SetVisAttributes(disk_col);
  for(int ii=0;ii<tubenum;ii++){
    log_tube[ii]->SetVisAttributes(tube_col);
    log_gas[ii]->SetVisAttributes(gas_col);
  }
  log_station->SetVisAttributes(G4VisAttributes::Invisible);
  log_asm->SetVisAttributes (G4VisAttributes::Invisible);
  //log_tube->SetVisAttributes (G4VisAttributes::Invisible);
  //

  /*
  log_gas0->SetVisAttributes(gas_col);
  log_gas1->SetVisAttributes(gas_col);
  log_gas2->SetVisAttributes(gas_col);
  log_gas3->SetVisAttributes(gas_col);
  log_gas4->SetVisAttributes(gas_col);
  log_disk0->SetVisAttributes(disk_col);
  log_disk1->SetVisAttributes(disk_col);
  log_disk2->SetVisAttributes(disk_col);
  log_disk3->SetVisAttributes(disk_col);
  log_disk4->SetVisAttributes(disk_col);
  */
  //
  //always return the physical World
  //
  return physiWorld;
}


void DetectorConstruction::PrintCalorParameters()
{
  /*
  G4cout << "\n------------------------------------------------------------"
         << "\n---> The calorimeter is " << NbOfLayers << " layers of: [ "
         << AbsorberThickness/mm << "mm of " << AbsorberMaterial->GetName() 
         << " + "
         << GapThickness/mm << "mm of " << GapMaterial->GetName() << " ] " 
         << "\n------------------------------------------------------------\n";
  */
}


void DetectorConstruction::SetAbsorberMaterial(G4String materialChoice)
{
  // search the material by its name   
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);     
  if (pttoMaterial) AbsorberMaterial = pttoMaterial;
}


void DetectorConstruction::SetDiskMaterial(G4String materialChoice){
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
  if(pttoMaterial) DiskMaterial = pttoMaterial;
}

void DetectorConstruction::SetGasMaterial(G4String materialChoice){
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
  if(pttoMaterial) GasMaterial = pttoMaterial;
}

void DetectorConstruction::SetGapMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
  if (pttoMaterial) GapMaterial = pttoMaterial;
}


void DetectorConstruction::SetAbsorberThickness(G4double val)
{
  // change Absorber thickness and recompute the calorimeter parameters
  AbsorberThickness = val;
}


void DetectorConstruction::SetGapThickness(G4double val)
{
  // change Gap thickness and recompute the calorimeter parameters
  GapThickness = val;
}


void DetectorConstruction::SetCalorSizeYZ(G4double val)
{
  // change the transverse size and recompute the calorimeter parameters
  CalorSizeYZ = val;
}


void DetectorConstruction::SetNbOfLayers(G4int val)
{
  NbOfLayers = val;
}

/*
void DetectorConstruction::SetMagField(G4double fieldValue)
{

  MField* myfield = new MField;
  //apply a global uniform magnetic field along Z axis
  G4FieldManager* fieldMgr
   = G4TransportationManager::GetTransportationManager()->GetFieldManager();

  if(magField) delete magField;		//delete the existing magn field

  //10/04/2012 added
  //  fieldValue = 1.0*tesla;


  fieldMgr->SetDetectorField(myfield);
  fieldMgr->CreateChordFinder(myfield);


  
  if(fieldValue!=0.)			// create a new one if non nul
    { magField = new G4UniformMagField(G4ThreeVector(0.,0.,fieldValue));
    fieldMgr->SetDetectorField(magField);
    fieldMgr->CreateChordFinder(magField);
  } else {
    magField = 0;
    fieldMgr->SetDetectorField(magField);
  }
  
}
*/

#include "G4RunManager.hh"

void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructCalorimeter());
}

