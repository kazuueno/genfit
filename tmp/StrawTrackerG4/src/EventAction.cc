#include "EventAction.hh"

#include "RunAction.hh"
#include "EventActionMessenger.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4UnitsTable.hh"

#include "Randomize.hh"
#include <iomanip>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction()
{

  runAct = (RunAction*)G4RunManager::GetRunManager()->GetUserRunAction();
  eventMessenger = new EventActionMessenger(this);
  printModulo = 1;
  evtNb=0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{
  delete eventMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event* evt)
{  

  evtNb = evt->GetEventID();
  //if (evtNb%printModulo == 0) { 
  if(evtNb%10 == 0){
    G4cout << "\n---> Begin of event: " << evtNb << G4endl;
    //CLHEP::HepRandom::showEngineStatus();
  }
 
  
 // initialisation per event
  // EnergyAbs = EnergyGap = 0.;
  //TrackLAbs = TrackLGap = 0.;
  Src_ene = 0;
  Src_pos = G4ThreeVector();
  Src_mom = G4ThreeVector();
  NradGas = 0;
  NradTube = 0;
  NstepGas = 0;
  NstepTube = 0;
  for(int ii=0; ii<20; ii++) gfla[ii]=0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* evt)
{
  //accumulates statistic
  //
  //runAct->fillPerEvent(EnergyAbs, EnergyGap, TrackLAbs, TrackLGap);
  
  //print per event (modulo n)
  //
  evtNb = evt->GetEventID();
  /*
  if (evtNb%printModulo == 0) {
    G4cout << "---> End of event: " << evtNb << G4endl;	
  }
  */
  std::ofstream fgf("outgasflag.dat",std::ios::app);
  fgf << evtNb;
  for(int ii=0; ii<20; ii++){
    if(ii%4==0) fgf << " ";
    if(gfla[ii]>0) fgf << 1 <<" ";
    if(gfla[ii]==0) fgf << 0 << " ";
  }
  fgf << G4endl;
  fgf.close();

  std::ofstream frad("outradlen.dat",std::ios::app);
  frad << evtNb << " " << NstepGas << " " << NradGas << " "
       << NstepTube << " " << NradTube << G4endl;
  frad.close();
  

}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
