#include "SteppingAction.hh"

#include "DetectorConstruction.hh"
#include "EventAction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"

#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4StepStatus.hh"
#include "G4LogicalVolume.hh"

SteppingAction::SteppingAction()					 
{
  detector = (DetectorConstruction*)
             G4RunManager::GetRunManager()->GetUserDetectorConstruction();
  eventaction = (EventAction*)
                G4RunManager::GetRunManager()->GetUserEventAction();	       
 
}


SteppingAction::~SteppingAction()
{ }


void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
  G4double tubel = 9.75+0.025;
  G4double gap = 1.;
  // get volume of the current step
  G4VPhysicalVolume* volume 
  = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
  G4VPhysicalVolume* postvolume 
  = aStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume();
  G4double edep = aStep->GetTotalEnergyDeposit();
  G4Track* track = aStep->GetTrack();
  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4ThreeVector pos = track->GetPosition();
  G4ThreeVector prepos = preStepPoint->GetPosition();
  G4ThreeVector premom = preStepPoint->GetMomentum();
  G4int track_id = track->GetTrackID();
  G4int step_num = track->GetCurrentStepNumber();
  G4ThreeVector pmome = track->GetMomentumDirection();
  G4ThreeVector pmome2 = track->GetMomentum();
  G4String process = aStep->GetPostStepPoint()
    ->GetProcessDefinedStep()->GetProcessName();
  G4ParticleDefinition* pd = aStep->GetTrack()->GetDefinition();
  G4String pname = pd->GetParticleName();
  G4double kE = aStep->GetPostStepPoint()->GetKineticEnergy();
  G4double pkE = aStep->GetPreStepPoint()->GetKineticEnergy();
  G4int evtnum = eventaction->GetEvtNb();
  
  G4String volname = volume->GetName();
  
  G4StepStatus stepIn = aStep->GetPreStepPoint()->GetStepStatus();
  G4StepStatus stepOut = aStep->GetPostStepPoint()->GetStepStatus();

  G4TouchableHistory *touchHist 
    = (G4TouchableHistory*)aStep->GetPreStepPoint()->GetTouchable();
  G4LogicalVolume *lv = touchHist->GetVolume()->GetLogicalVolume();
  G4double steplen = aStep->GetStepLength();
  //  G4Material* material = preStepPoint->GetMaterial();
  G4Material* material = lv->GetMaterial();
  G4double radlen = material->GetRadlen();

  G4double gtime = track->GetGlobalTime();
  if(gtime>40000){
    G4cout << "------- time over --------" << G4endl;
    track->SetTrackStatus(fStopAndKill);
  }

  G4double slen = steplen/cm;
  G4double rlen = radlen/cm;

  G4double pmom[3],pmom2[3],ppos[3],prpos[3],Ene,pEne,dpen;
  Ene = kE/MeV; pEne = pkE/MeV;
  dpen = edep/MeV;
  pmom[0]=pmome.x(); pmom[1]=pmome.y(); pmom[2]=pmome.z();
  pmom2[0]=pmome2.x(); pmom2[1]=pmome2.y(); pmom2[2]=pmome2.z();
  ppos[0]=pos.x()/mm; ppos[1]=pos.y()/mm; ppos[2]=pos.z()/mm;
  prpos[0]=prepos.x()/mm;prpos[1]=prepos.y()/mm;prpos[2]=prepos.z()/mm;
  
  if(track_id==1){
    if(step_num==1){
      eventaction->Src_Ene(pEne);
      eventaction->Src_Pos(prepos);
      eventaction->Src_Mom(premom);
      std::ofstream strawvol("outvol.dat",std::ios::app);
      strawvol << evtnum << " " << 0 << " " 
	       << prpos[0] << " " << prpos[1] << " " << prpos[2] << " " 
	       << premom[0] << " " << premom[1] << " " << premom[2] << " "
	       << G4endl;
      strawvol.close();
      //G4cout << "---momentum  " << sqrt(sqr(pmom2[0])+sqr(pmom2[1])+sqr(pmom2[2])) << G4endl;
    }
  }
  
  if(track_id==1){
    if(volname=="gas" && (stepIn==fGeomBoundary || stepOut==fGeomBoundary)){
      G4int steptype=-999;
      if(stepIn==fGeomBoundary && stepOut!=fGeomBoundary) steptype=1;
      if(stepIn!=fGeomBoundary && stepOut==fGeomBoundary) steptype=-1;
      std::ofstream strawvol("outvol.dat",std::ios::app);
      strawvol << evtnum << " " << steptype << " ";
      if(steptype==1)
	strawvol<<prepos[0]<<" "<<prepos[1]<<" "<<prepos[2]<<" "<<premom[0]<<" "<<premom[1]<<" "<<premom[2];
      else
	strawvol<<pos[0]<<" "<<pos[1]<<" "<<pos[2]<<" "<<pmome2[0]<<" "<<pmome2[1]<<" "<<pmome2[2];
      
      strawvol<< G4endl;
      strawvol.close();
      //G4cout << "---momentum  " << sqrt(sqr(pmom2[0])+sqr(pmom2[1])+sqr(pmom2[2])) << G4endl;
    }

    if(volname=="gas" && dpen>0){
      for(int jj=0; jj<5; jj++){
	//for(int ii=0; ii<4; ii++){
	  if(-1.5*gap-tubel*2+480*jj<ppos[2]&& ppos[2]<-1.5*gap-tubel+480*jj) eventaction->gfla[jj*4]++;
	  if(-0.5*gap-tubel+480*jj<ppos[2]&& ppos[2]<-0.5*gap+480*jj) eventaction->gfla[1+jj*4]++;
	  if(0.5*gap+480*jj<ppos[2]&& ppos[2]<0.5*gap+tubel+480*jj) eventaction->gfla[2+jj*4]++;
	  if(1.5*gap+tubel+480*jj<ppos[2]&& ppos[2]<1.5*gap+tubel*2+480*jj) eventaction->gfla[3+jj*4]++;
	  //}
      }
    }

    if(volname=="gas") eventaction->SumRadLengthGas(slen,slen/rlen);
    if(volname=="tube") eventaction->SumRadLengthTube(slen,slen/rlen);
    /*
    if(volname=="gas") G4cout << steplen << " " << slen << " "
			      << radlen << " " <<rlen << G4endl;
    */
  }
  
  if(track_id==1 && pname=="e-" && volname=="gas"){
    std::ofstream strawtmp("outpos.dat",std::ios::app);
    strawtmp << ppos[0] << " " << ppos[1] << " " << ppos[2] << G4endl;
    strawtmp.close();
  }
  


  //example of saving random number seed of this event, under condition
  //// if (condition) G4RunManager::GetRunManager()->rndmSaveThisEvent(); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
