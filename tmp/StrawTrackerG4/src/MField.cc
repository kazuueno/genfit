#include "MField.hh"

MField::MField(){
  Bz = 1.0*tesla;
  rmax_sq = sqr(1.68/2*m);
}

MField::~MField(){;}

void MField::GetFieldValue(const G4double Point[3], G4double* Bfield) const
{

  //    const G4double Bzz= 1.0*tesla;
  //    const G4double rmax_sqq = sqr(1.3*m);
  
  Bfield[0]= 0.;
  Bfield[1] = 0.;
  if((sqr(Point[0])+sqr(Point[1]))< rmax_sq) {
    Bfield[2]= Bz;
  } else { 
    Bfield[2]= 0.; 
  }

  return;

}
