#!/bin/bash

function makeconfig(){
    nstations=$1
    wall=$2
    gas=$3
#/det/setGasMaterial Helium50_Ethane50
#/det/setGasMaterial Argon50_Ethane50

    echo "
/det/setNStations "$nstations"
/det/setWallThickness "$wall" mm
/det/setGasMaterial "$gas"
/det/update
/run/beamOn 20000
" >run.mac

}

echo "start :"`date`


makeconfig 5 0.025 Argon50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.025_st5_Ar
mv *.dat *.eps *.gif *.gdml ../result/wall0.025_st5_Ar

makeconfig 5 0.025 Helium50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.025_st5_He
mv *.dat *.eps *.gif *.gdml ../result/wall0.025_st5_He

makeconfig 5 0.02 Argon50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.02_st5_Ar
mv *.dat *.eps *.gif *.gdml ../result/wall0.02_st5_Ar


makeconfig 5 0.0125 Argon50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.0125_st5_Ar
mv *.dat *.eps *.gif *.gdml *.root ../result/wall0.0125_st5_Ar

makeconfig 6 0.025 Argon50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.025_st6_Ar
mv *.dat *.eps *.gif *.gdml *.root ../result/wall0.025_st6_Ar

makeconfig 7 0.025 Argon50_Ethane50
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.025_st7_Ar
mv *.dat *.eps *.gif *.gdml *.root ../result/wall0.025_st7_Ar

makeconfig 5 0.0001 Vacuum
./straw02_5 run.mac
mv outvol.dat outvol0.dat
mv outgasflag.dat outgasflag0.dat
mv outradlen.dat outradlen0.dat
./result.sh
mkdir ../result/wall0.0001_st5_Vac
mv *.dat *.eps *.gif *.gdml *.root ../result/wall0.0001_st5_Vac


echo end   : `date`
