#!/bin/sh

root -l -b <<EOF
.x ../macro/HitNum.C("$1")
.q
EOF

root -l -b <<EOF
.x ../macro/RadLen.C("$1")
EOF

root -l <<EOF
.x ../macro/Conv.C
.q
EOF

../macro/ana/main $1

