#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class RunAction;
class EventActionMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EventAction : public G4UserEventAction
{
public:
  EventAction();
  virtual ~EventAction();

  void  BeginOfEventAction(const G4Event*);
  void    EndOfEventAction(const G4Event*);

  G4int GetEvtNb() {return evtNb;};
    
  void AddAbs(G4double de, G4double dl) {EnergyAbs += de; TrackLAbs += dl;};
  void AddGap(G4double de, G4double dl) {EnergyGap += de; TrackLGap += dl;};
                     
  void SetPrintModulo(G4int    val)  {printModulo = val;};
  
  void Src_Ene(G4double ene) {Src_ene = ene;};
  void Src_Pos(G4ThreeVector vec) {Src_pos = vec;};
  void Src_Mom(G4ThreeVector vec) {Src_mom = vec;};

  void SumRadLengthGas(G4double sl, G4double rl){NstepGas += sl; NradGas +=rl;};
  void SumRadLengthTube(G4double sl, G4double rl){NstepTube += sl; NradTube +=rl;};

  G4int gfla[20];
    
private:
   RunAction*  runAct;
   
   G4int evtNb;

   G4double  EnergyAbs, EnergyGap;
   G4double  TrackLAbs, TrackLGap;

  G4double Src_ene;
  G4ThreeVector Src_pos;
  G4ThreeVector Src_mom;

  G4double NstepGas,NradGas;
  G4double NstepTube,NradTube;
                     
   G4int     printModulo;
                             
   EventActionMessenger*  eventMessenger;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
