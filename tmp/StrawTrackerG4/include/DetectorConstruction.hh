#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4Box;
class G4Tubs;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
//class G4UniformMagField;
class DetectorMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
  
    DetectorConstruction();
   ~DetectorConstruction();

  public:
     
     void SetDiskMaterial(G4String);
     void SetGasMaterial(G4String);

     void SetAbsorberMaterial (G4String);     
     void SetAbsorberThickness(G4double);     

     void SetGapMaterial (G4String);     
     void SetGapThickness(G4double);
     
     void SetCalorSizeYZ(G4double);          
     void SetNbOfLayers (G4int);   
     void SetNStations (G4int nstations){NStations=nstations;}   
     void SetWallThickness(G4double th){WallThickness=th;}
      
  //void SetMagField(G4double);
     
     G4VPhysicalVolume* Construct();

     void UpdateGeometry();
     
  public:
  
     void PrintCalorParameters(); 
                    
     G4double GetWorldSizeX()           {return WorldSizeX;}; 
     G4double GetWorldSizeYZ()          {return WorldSizeYZ;};
     
     G4double GetCalorThickness()       {return CalorThickness;}; 
     G4double GetCalorSizeYZ()          {return CalorSizeYZ;};
      
     G4int GetNbOfLayers()              {return NbOfLayers;}; 

     G4Material* GetDiskMaterial() {return DiskMaterial;};
     G4Material* GetGasMaterial() {return GasMaterial;};
     
     G4Material* GetAbsorberMaterial()  {return AbsorberMaterial;};
     G4double    GetAbsorberThickness() {return AbsorberThickness;};      
     
     G4Material* GetGapMaterial()       {return GapMaterial;};
     G4double    GetGapThickness()      {return GapThickness;};
     
     const G4VPhysicalVolume* GetphysiWorld() {return physiWorld;};           
     const G4VPhysicalVolume* GetAbsorber()   {return physiAbsorber;};
     const G4VPhysicalVolume* GetGap()        {return physiGap;};

  //     const G4VPhysicalVolume* GetGas()        {return phys_gas;};
                 
  private:
     
     G4Material*        DiskMaterial;
     G4Material*        GasMaterial;

     G4Material*        AbsorberMaterial;
     G4double           AbsorberThickness;
     
     G4Material*        GapMaterial;
     G4double           GapThickness;
     
     G4int              NbOfLayers;
     G4double           LayerThickness;
          
     G4double           CalorSizeYZ;
     G4double           CalorThickness;

     double WallThickness;
     int                NStations;
     
     G4Material*        defaultMaterial;
     G4double           WorldSizeYZ;
     G4double           WorldSizeX;

     G4Tubs*            sol_station;
     G4LogicalVolume*   log_station;
     G4VPhysicalVolume* phys_station[10];     

     G4Tubs*            sol_asm;
     G4LogicalVolume*   log_asm;
     G4VPhysicalVolume* phys_asm[4];     

     G4Tubs*            sol_tube[96*4];
     G4LogicalVolume*   log_tube[96*4];
     G4VPhysicalVolume* phys_tube[96*4];     

     G4Tubs*            sol_gas[96*4];
     G4LogicalVolume*   log_gas[96*4];
     G4VPhysicalVolume* phys_gas[96*4];

     G4Tubs*            sol_sol;
     G4LogicalVolume*   log_sol;
     G4VPhysicalVolume* phys_sol;

     G4Tubs*            sol_disk0;
     G4LogicalVolume*   log_disk0;
     G4VPhysicalVolume* phys_disk0;

     G4Tubs*            sol_gas0;
     G4LogicalVolume*   log_gas0;
     G4VPhysicalVolume* phys_gas0;

     G4Tubs*            sol_disk1;
     G4LogicalVolume*   log_disk1;
     G4VPhysicalVolume* phys_disk1;

     G4Tubs*            sol_gas1;
     G4LogicalVolume*   log_gas1;
     G4VPhysicalVolume* phys_gas1;

     G4Tubs*            sol_disk2;
     G4LogicalVolume*   log_disk2;
     G4VPhysicalVolume* phys_disk2;

     G4Tubs*            sol_gas2;
     G4LogicalVolume*   log_gas2;
     G4VPhysicalVolume* phys_gas2;

     G4Tubs*            sol_disk3;
     G4LogicalVolume*   log_disk3;
     G4VPhysicalVolume* phys_disk3;

     G4Tubs*            sol_gas3;
     G4LogicalVolume*   log_gas3;
     G4VPhysicalVolume* phys_gas3;

     G4Tubs*            sol_disk4;
     G4LogicalVolume*   log_disk4;
     G4VPhysicalVolume* phys_disk4;

     G4Tubs*            sol_gas4;
     G4LogicalVolume*   log_gas4;
     G4VPhysicalVolume* phys_gas4;

     G4Box*             solidWorld;    //pointer to the solid World 
     G4LogicalVolume*   logicWorld;    //pointer to the logical World
     G4VPhysicalVolume* physiWorld;    //pointer to the physical World

     G4Box*             solidCalor;    //pointer to the solid Calor 
     G4LogicalVolume*   logicCalor;    //pointer to the logical Calor
     G4VPhysicalVolume* physiCalor;    //pointer to the physical Calor
     
     G4Box*             solidLayer;    //pointer to the solid Layer 
     G4LogicalVolume*   logicLayer;    //pointer to the logical Layer
     G4VPhysicalVolume* physiLayer;    //pointer to the physical Layer
         
     G4Box*             solidAbsorber; //pointer to the solid Absorber
     G4LogicalVolume*   logicAbsorber; //pointer to the logical Absorber
     G4VPhysicalVolume* physiAbsorber; //pointer to the physical Absorber
     
     G4Box*             solidGap;      //pointer to the solid Gap
     G4LogicalVolume*   logicGap;      //pointer to the logical Gap
     G4VPhysicalVolume* physiGap;      //pointer to the physical Gap
     
  //G4UniformMagField* magField;      //pointer to the magnetic field
     
     DetectorMessenger* detectorMessenger;  //pointer to the Messenger
      
  private:
    
     void DefineMaterials();
     void ComputeCalorParameters();
     G4VPhysicalVolume* ConstructCalorimeter();     
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void DetectorConstruction::ComputeCalorParameters()
{
  // Compute derived parameters of the calorimeter
     LayerThickness = AbsorberThickness + GapThickness;
     CalorThickness = NbOfLayers*LayerThickness;
     
     WorldSizeX = 1.2*CalorThickness; WorldSizeYZ = 1.2*CalorSizeYZ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

