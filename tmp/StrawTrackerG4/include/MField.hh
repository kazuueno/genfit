#ifndef M_FIELD_H
#define M_FIELD_H

#include "globals.hh"
#include "G4MagneticField.hh"

class MField : public G4MagneticField {
public:
  MField();
  ~MField();
  
  void GetFieldValue(const  G4double Point[3],  G4double* Bfield ) const;

private:
  G4double Bz;
  G4double rmax_sq;
  
};

#endif
