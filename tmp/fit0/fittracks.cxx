#include "TApplication.h"
#include "TRint.h"
#include "TGeoManager.h"
#include "TRandom.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TGeoTube.h"
#include "TH1D.h"
#include "TCanvas.h"

#include "GFException.h"
#include "GFAbsTrackRep.h"
#include "RKTrackRep.h"
#include "GFConstField.h"
#include "GFFieldManager.h"
#include <GFTGeoMaterialInterface.h>
#include <GFMaterialEffects.h>

#include "WireHit.h"
#include "WirePointHit.h"
#include "SpacepointHit.h"
#include "ProlateSpacepointHit.h"
#include "PixHit.h"
#include "GFTrack.h"
#include "GFKalman.h"
#include "math.h"

#include <fstream>
#include <getopt.h>

#include "recEvent.h"
recEvent event;

double chi2;
TFile *file;
TTree *tree;
TH1D *hpull[6];
TH1D *hres[7];
TH1D *hchi2;


TVector3 xyz0,mom0,xyzstart,momstart;
std::vector<StrawHit> hits;
double sigmadist=0.02;

void initfitter(std::string outfile){
  GFFieldManager::getInstance()->init(new GFConstField(0.,0.,10.0));
  GFMaterialEffects* mateff=GFMaterialEffects::getInstance();
  //mateff->setEnergyLossBetheBloch(true);
  //mateff->setNoiseBetheBloch(true);
  //mateff->setNoiseCoulomb(true);
  mateff->setEnergyLossBrems(false);
  mateff->setNoiseBrems(false);
  //mateff->setMscModel("GEANE");

  GFMaterialEffects::getInstance()->init(new GFTGeoMaterialInterface());

  file = TFile::Open(outfile.c_str(),"RECREATE");
  tree = new TTree("t","example output");
  
  tree->Branch("ev",&event);

  double hxrange[7]={10.,1,0.1,0.1,1,1,5};
  const char* ptitle[6]={"pull P;pull p","Pull 1/P;pull 1/p;","pull ;pull px/pz;",";pull py/pz;",";pull x;",";pull y;"};
  const char* rtitle[7]={"resolution P;P-Pmc, MeV","resolution 1/P;1/P-1/Pmc, 1/GeV","resolution px/pz;#Delta px/pz;","resolution py/pz;#Delta py/pz;","resolution x;X-Xmc, cm;","resolution Y,Y-Ymc, cm;","resolution Pt;Pt-Ptmc,GeV"};
  for(int i=0;i<6;i++){
    hpull[i]=new TH1D(Form("hpull%i",i),ptitle[i],500,-10,10);
    hres[i]=new TH1D(Form("hres%i",i),rtitle[i],500,-hxrange[i],hxrange[i]);
  }
  hres[6]=new TH1D(Form("hres%i",6),rtitle[6],100,-hxrange[6],hxrange[6]);
  hchi2=new TH1D("hchi2","chi2;#chi^{2}/ndf",1000,0,100);
  
}


void fittrack(int iev){
  TMatrixDSym Cov(6); Cov.Zero();
  Cov[0][0] = Cov[1][1] =Cov[2][2] = .01; //centimeter^2
  //Cov[3][3] = Cov[4][4] =Cov[5][5] = 0.001*0.001; //GeV^2

  double sigma_p0=0.001;
  double sigma_angle0=0.1;
  
  TVector3 dirP=mom0;dirP=dirP.Unit();
  TVector3 zaxis(0,0,1);
  TVector3 v_axis = dirP.Cross(zaxis).Unit();
  TVector3 u_axis = dirP.Cross(v_axis).Unit();
  TVector3 axis[3]={dirP,v_axis,u_axis};
  TMatrixD hrot(3,3);
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++) hrot(i,j)=axis[j](i);
  TMatrixD cov0(3,3);
  cov0(0,0)=pow(sigma_p0,2.);
  cov0(1,1)=pow(sigma_angle0*mom0.Mag(),2.);
  cov0(2,2)=pow(sigma_angle0*mom0.Mag(),2.);
  TMatrixD covrot(3,3);
  covrot=hrot*cov0;covrot*=hrot.T();
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++) Cov[i+3][j+3]=covrot(i,j);

  TVector3 xyz0sm=xyz0;
  TVector3 mom0sm=mom0;

  for(int i=0;i<3;i++) xyz0sm[i]+=gRandom->Gaus()*0.1;
  mom0sm.SetMag(mom0sm.Mag()+gRandom->Gaus()*sigma_p0);
  mom0sm.Rotate(gRandom->Gaus()*sigma_angle0,u_axis);
  mom0sm.Rotate(gRandom->Gaus()*sigma_angle0,v_axis);
  
  GFAbsTrackRep *rep = new RKTrackRep(xyz0sm,
                                      mom0sm,
                                      Cov,
                                      11);

  GFAbsTrackRep *rep0 = new RKTrackRep(xyz0,//start,
				       mom0,//start,
				       Cov,
				       11);
  double refplaneZ=-3;//cm,
  //  GFDetPlane refplane = rep0->getReferencePlane();
  GFDetPlane refplane(TVector3(0,0,refplaneZ),zaxis);
  rep0->extrapolate(refplane);

  //  std::cout<<"refplane u= "<<refplane.getU()<<" v= "<<refplane.getV()<<std::endl;
  // std::cout<<"init xyz0 "<<xyz0<<" P "<<mom0.Mag()<<" theta "<<mom0.Theta()<<" ph "<<mom0.Phi()<<std::endl;

  event.clear();
  //for saving of MC truth    
  event.stMCT.ResizeTo(rep0->getState());
  event.stMCT = rep0->getState();
  event.ev=iev;


  GFTrack fitTrack(rep);
  int nhits=hits.size();
  double prevz=xyz0[2];
  int nvirtplanes=0;
  int nhitsadd=0;
  for(int i=0;i<nhits;i++){
    //add virtual planes if distance to long
    if(fabs(hits[i].wire0[2]-prevz)>5.){
      PixHit* vrthit=new PixHit(TVector3(0,0,prevz+1.),TVector3(0,0,1),TVector3(1,0,0),1e30);
      fitTrack.addHit(vrthit,5,200+i);
      nvirtplanes++;
      PixHit* vrthit2=new PixHit(TVector3(0,0,hits[i].wire0[2]-1.),TVector3(0,0,1),TVector3(1,0,0),1e30);
      fitTrack.addHit(vrthit2,5,100+i);
      nvirtplanes++;
    }
    prevz=hits[i].wire0[2];
    //SpacepointHit* vrthit=new SpacepointHit(hits[i].xyz,1e30);
    //fitTrack.addHit(vrthit,5,100+i);
    event.hits.push_back(hits[i]);
    //    if(int(hits[i].station/2)==0) continue;
    if(sigmadist>1){
      sigmadist=1./sqrt(12.);
      hits[i].dist=0;
    }
    WireHit *whit=new WireHit(hits[i].wire0,hits[i].wire1,hits[i].dist,sigmadist);
    int leftrigth=(hits[i].wire1-hits[i].wire0).Cross(hits[i].mom).Dot(hits[i].xyz-hits[i].wire0)>0?-1:1;
    whit->setLeftRightResolution(leftrigth);
    nhitsadd++;
    
    fitTrack.addHit(whit,3/*dummy detector id*/,i);

  }
  //  std::cout<<"ntotal mes "<<nhits+nvirtplanes<<" nhits "<<nhits<<" nvirtual "<<nvirtplanes<<std::endl;


  GFKalman k;
  k.setNumIterations(3);
  try{
    k.processTrack(&fitTrack);
  }catch(GFException& e){e.what();}

  //  if(rep->getStatusFlag() != 0 ) return;
  try{
    if(rep->getStatusFlag()==0)
      rep->extrapolate(refplane);
  }catch(GFException& e){e.what();}


  event.stREC.ResizeTo(rep->getState());
  event.stREC = rep->getState();
  //    stREC->Print();
  event.covREC.ResizeTo(rep->getCov());
  event.covREC = rep->getCov();
  event.nfail = fitTrack.getFailedHits();
  double chi2 = rep->getRedChiSqu()*rep->getNDF()/(rep->getNDF()-nvirtplanes*2);
  
  event.chi2=chi2;
  event.nhits=nhitsadd;
  event.status=rep->getStatusFlag();
  if(hits.size()){
    event.momin=hits[0].mom.Mag();
    event.momout=hits[hits.size()-1].mom.Mag();
  }
  event.posMC=xyz0;
  event.momMC=mom0;
  event.posstart=xyzstart;
  event.momstart=momstart;
  rep0->getPosMom(refplane,event.posMC,event.momMC);
  delete rep0;

  if(rep->getStatusFlag() == 0 && event.nfail==0){
    try{
      rep->getPosMomCov(refplane,event.pos,event.mom,event.cov);

    }catch(GFException& e){e.what();}
  }
  
  tree->Fill();

  double momrec=1./event.stREC(0)*1000;
  double mommc=1./event.stMCT(0)*1000;
  double sigma_p=momrec*momrec*TMath::Sqrt(event.covREC[0][0])/1000;
  std::cout<<iev<<" chi2 "<<chi2
	   <<" ndf "<<rep->getNDF()
	   <<" nfailed "<<fitTrack.getFailedHits()<<" status "<<rep->getStatusFlag()
	   <<" P= "<<momrec<<" +- "<<sigma_p<<" mc "<<mommc<<std::endl;

  if(rep->getStatusFlag() != 0 || event.nfail>0) return;

  for(int i=0;i<5;i++){
    double sigma=TMath::Sqrt(event.covREC[i][i]);
    double valrec=event.stREC[i];
    double valmc=event.stMCT[i];
    hpull[i+1]->Fill((valrec-valmc)/sigma);
    hres[i+1]->Fill(valrec-valmc);
  }
  hpull[0]->Fill((momrec-mommc)/sigma_p);
  hres[0]->Fill(momrec-mommc);
  hres[6]->Fill((event.mom.Pt()-mom0.Pt())*1000);
  hchi2->Fill(chi2);

}

void endfitter(){
  TCanvas *c=new TCanvas;
  c->Draw();
  c->Divide(2,3,0.001,0.001);
  for(int i=0;i<6;i++){
    c->cd(i+1);
    hpull[i]->Fit("gaus");
  }
  TCanvas *c2=new TCanvas;
  c2->Draw();
  c2->Divide(2,3,0.001,0.001);
  for(int i=0;i<6;i++){
    c2->cd(i+1);
    hres[i]->Fit("gaus");
    if(i==5) hres[i]->Fit("gaus","r","",-5,5);
  }
  c->Write();
  c2->Write();
  hres[6]->Fit("gaus");

  tree->Write();
  file->Write();
  file->Close();
}
void usage(){
  std::cout<<
"Usage: fittracks [options] \n\
Options:\n\
  -o, --output   <arg> set output file\n\
  -i, --input    <arg> output file \n\
  -g, --geometry <arg> geometry \n"<<std::endl;
}
int main(int argc, char **argv)
{
  static struct option long_options[] = {
    {"input"   , 1, 0, 'i'},
    {"geometry", 1, 0, 'g'},
    {"output"  , 1, 0, 'o'},
    {"resolution"  , 1, 0, 'r'},
    {0, 0, 0, 0}
  };
  int c;
  std::string input="outvol0.dat";
  std::string output="out.root";
  std::string geomfile="geom25mum.gdml";
  sigmadist=0.02;
  while(1){
    int option_index = 0;
    c=getopt_long(argc,argv,"i:o:g:r:",long_options, &option_index);
    if(c == -1) break;
    switch (c) {
    case 0: break;
    case 'i': if(optarg) input=optarg;   break;
    case 'o': if(optarg) output=optarg;   break;
    case 'g': if(optarg) geomfile=optarg;   break;
    case 'r': if(optarg) sigmadist=atof(optarg);   break;
    default: usage(); break;
    }
  }
  std::cout<<"Config geom "<<geomfile<<" input "<<input<<" output "<<output<<" distsigma "<<sigmadist<<std::endl;
  TRint theApp("Rint", &argc, argv);
  TGeoManager *geo=TGeoManager::Import(geomfile.c_str());
  initfitter(output);

  std::ifstream in(input.c_str());
  TVector3 mom,xyz,momin,xyzin,momout,xyzout;
  int iev,volstatus;
  int prevstatus;
  bool firstevent=true;
  bool firstwire=true;

  while(in>>iev>>volstatus>>xyz[0]>>xyz[1]>>xyz[2]>>mom[0]>>mom[1]>>mom[2]){
    xyz*=1./10.;
    mom*=1./1000.;
    if(volstatus==0){
      if(!firstevent) {
	//	if(iev==4)
	fittrack(iev-1);
	//if(iev==5) break;
      }
      hits.clear();
      momstart=mom;
      xyzstart=xyz;
      firstevent=false;
      firstwire=true;
    }else if(volstatus==1){
      momin=mom;
      xyzin=xyz;
      if(firstwire){
	mom0=mom;
	xyz0=xyz;
      }
      firstwire=false;
    }else if(volstatus==-1){
      if(prevstatus!=1){
	std::cout<<"something wrong no enterance to straw volume iev="<<iev<<std::endl;
	continue;
      }
      momout=mom;
      xyzout=xyz;
      xyz=0.5*(xyzin+xyzout);
      TGeoNode *gnode=geo->FindNode(xyz[0],xyz[1],xyz[2]);
      std::string volname=gnode->GetVolume()->GetName();
      if(volname!="gas") {
	std::cout<<"something wrong not straw volume name "<<volname<<" iev="<<iev<<" "
		 <<xyzin[0]<<" "<<xyzin[1]<<" "<<xyzin[2]<<" "<<xyzout[0]<<" "<<xyzout[1]<<" "<<xyzout[2]<<std::endl;
	continue;
      }


      TGeoTube *gtube=dynamic_cast<TGeoTube*>(gnode->GetVolume()->GetShape());
      if(!gtube){
	std::cout<<"something wrong not tube volume iev="<<iev<<std::endl;
	gnode->GetVolume()->GetShape()->Dump();
	continue;
      }
      StrawHit hit;
      
      double wire0[3],wire1[3];
      double dz=gtube->GetDz();
      double ptlocal[3]={0,0,-dz};
      geo->LocalToMaster(ptlocal,wire0);
      ptlocal[2]*=-1;
      geo->LocalToMaster(ptlocal,wire1);
      //std::cout<<"wire "<<wire0[0]<<" "<<wire0[1]<<" "<<wire0[2]
      // 	       <<" "<<wire1[0]<<" "<<wire1[1]<<" "<<wire1[2]<<std::endl;
      
      hit.wire0=TVector3(wire0);
      hit.wire1=TVector3(wire1);

      
      TVector3 k=momin.Unit();
      TVector3 wire_dir=(hit.wire1-hit.wire0).Unit();
      double smin=(k.Dot(wire_dir)*(wire_dir.Dot(xyzin - hit.wire0)) - k.Dot(xyzin - hit.wire0))/
	(1 - pow(k.Dot(wire_dir),2.));
      TVector3 xyzmom=xyzin+k*smin;
      TVector3 norm=k.Cross(wire_dir);norm=norm.Unit();
      double distmom=(xyzin-hit.wire0).Dot(norm);

      // TVector3 B(0,0,1);
      // TVector3 xyzmom2=xyzmom;//+B.Cross(momin)*(1./momin.Pt()*smin*smin/2/(momin.Pt()/3*1000.));
      // std::cout<<momin<<" "<<momout<<" "<<B.Cross(momin)<<" smin "<<smin<<std::endl;
      double axyz[3],amidlocxyz[3];
      double ainlocxyz[3],aoutlocxyz[3];
      xyz.GetXYZ(axyz);
      geo->MasterToLocal(axyz,amidlocxyz);
      TVector3 locmidxyz(amidlocxyz);
      xyzin.GetXYZ(axyz);
      geo->MasterToLocal(axyz,ainlocxyz);
      xyzout.GetXYZ(axyz);
      geo->MasterToLocal(axyz,aoutlocxyz);


      double alocxyz[3],alocxyz2[3];
      xyzmom.GetXYZ(axyz);
      geo->MasterToLocal(axyz,alocxyz);
      TVector3 locxyz(alocxyz);
      /*      std::cout<<iev<<" "<<hits.size()
	       <<" wirez "<<wire0[2]<<" xyz "<<axyz[0]<<" "<<axyz[1]<<" "<<xyz[2]
               <<" mom "<<momin.Mag()<<" dir "<<k[0]<<" "<<k[1]<<" "<<k[2]
               <<" dist "<<locmidxyz.Pt()<<" momest "<<distmom
               <<" locxyz "<<locxyz[0]<<" "<<locxyz[1]<<" "<<locxyz[2]
               <<" locinxyz "<<ainlocxyz[0]<<" "<<ainlocxyz[1]<<" "<<ainlocxyz[2]
               <<" locoutxyz "<<aoutlocxyz[0]<<" "<<aoutlocxyz[1]<<" "<<aoutlocxyz[2]<<std::endl;
      */
      hit.distmc=locxyz.Pt();
      hit.dist=hit.distmc+gRandom->Gaus()*sigmadist;
      hit.locz=locxyz[2];
      hit.xyz=TVector3(xyz);
      hit.mom=momin;
      hit.station=int((xyz[2]+20.)/48.)*2+(fabs(wire_dir.X())>fabs(wire_dir.Y()));

      hits.push_back(hit);
    }
    prevstatus=volstatus;
  }
  fittrack(iev);

  endfitter();
}
